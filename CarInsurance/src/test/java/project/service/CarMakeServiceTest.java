package project.service;

import project.models.CarMake;
import project.repositories.common.CarMakeRepository;
import project.services.CarMakeServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarMakeServiceTest {

    @InjectMocks
    CarMakeServiceImpl carMakeService;

    @Mock
    CarMakeRepository carMakeRepository;

    private CarMake testCarMake;
    private List<CarMake> testCarMakes;

    public static CarMake getCarMake() {
        return new CarMake(1, "Audi");
    }

    @BeforeEach
    public void init() {
        testCarMake = getCarMake();
        testCarMakes = Collections.singletonList(testCarMake);
    }

    @Test
    public void getOne_ShouldReturnAudi(){
        Mockito.when(carMakeRepository.getOne(Mockito.anyInt()))
                .thenReturn(testCarMake);

//        Act
        CarMake carMake1 = carMakeService.getById(testCarMake.getId());

        //Assert
        Assertions.assertEquals(testCarMake.getId(), carMake1.getId());
        Assertions.assertEquals(testCarMake.getName(), carMake1.getName());
    }

    @Test
    public void getAll_ShouldReturnAllBrands(){
        when(carMakeRepository.findAll()).thenReturn(Collections.singletonList(testCarMake));
        List<CarMake> actualResult = carMakeService.getAll();
        assertThat(actualResult).containsOnly(testCarMake);
    }
}
