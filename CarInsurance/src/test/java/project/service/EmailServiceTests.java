package project.service;

import project.services.EmailServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTests {

    @Mock
    JavaMailSender javaMailSender;

    @InjectMocks
    EmailServiceImpl emailService;

    @Test
    public void sendSimpleMessage_shouldSendMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("safetycar@gmail.com");
        message.setTo("to");
        message.setSubject("subject");
        message.setText("text");
        javaMailSender.send(message);

        //Act
        emailService.sendSimpleMessage(message.getReplyTo(), message.getSubject(), message.getText());
        //Assert
        Mockito.verify(javaMailSender, Mockito.times(1)).send(message);
    }
}
