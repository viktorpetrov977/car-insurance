package project.service;

import project.models.User;
import project.models.UserDetails;
import project.repositories.common.ConfirmationTokenRepository;
import project.repositories.common.UserRepository;
import project.services.UserServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    ConfirmationTokenRepository confirmationTokenRepository;

    @Test
    public void create_ShouldCreate_User() {
        //Arrange
        UserDetails testUserDetails = new UserDetails();

        //Act
        userService.create(testUserDetails);
        //Assert

        Mockito.verify(mockUserRepository, Mockito.times(1)).create(testUserDetails);
    }

    @Test
    public void getById_ShouldReturnUserWithGivenId() {
        //Arrange
        UserDetails testUserDetails = new UserDetails();

        //Act
        userService.getById(testUserDetails.getId());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getById(testUserDetails.getId());
    }

    @Test
    public void getByUsername_ShouldReturnUserWithGivenUsername() {
        //Arrange
        UserDetails testUserDetails = new UserDetails();

        //Act
        userService.getByUsername(testUserDetails.getUsername());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getByUsername(testUserDetails.getUsername());
    }

    @Test
    public void getByUsernameFromUser_ShouldReturnUserWithGivenUsername() {
        //Arrange
        User testuser = new User();

        //Act
        userService.getByUsernameFromUser(testuser.getUsername());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getByUsernameFromUser(testuser.getUsername());
    }
}

