package project.service;

import project.models.VehicleImage;
import project.repositories.common.FileDBRepository;
import project.services.FileStorageServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
public class FileStorageServiceTests {

    @Mock
    FileDBRepository fileDBRepository;

    @InjectMocks
    FileStorageServiceImpl fileStorageService;

    MultipartFile testFile;

    VehicleImage testImage;

    public static VehicleImage getVehicleImage() {
        return new VehicleImage();
    }

    @BeforeEach
    void init() {
        testFile = new MockMultipartFile("testFile", new byte[]{});
        testImage = getVehicleImage();
    }

    @Test
    public void createImage_ShouldCreate() throws IOException {
        //arrange
        //act
        fileStorageService.store(testFile);
        //assert
        Mockito.verify(fileDBRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void getOne_ShouldGetImage() {
        //arrange
        Mockito.when(fileDBRepository.findById(Mockito.anyInt()))
                .thenReturn(java.util.Optional.of(testImage));
        //act
        VehicleImage actual = fileStorageService.getFile(Mockito.anyInt());
        //assert
        Assertions.assertEquals(testImage, actual);
    }
//
//    @Test
//    public void getOne_ShouldThrow_whenMissingImage() {
//        //arrange
//        Mockito.when(fileDBRepository.findById(Mockito.anyInt()))
//                .thenReturn(java.util.Optional.empty());
//        //act assert
//        Assertions.assertThrows(EntityNotFoundException.class,
//                () -> fileStorageService.getFile(Mockito.anyInt()));
//    }

    @Test
    public void getAll_ShouldReturnCollection() {
        //arrange
        List<VehicleImage> expected = Collections.singletonList(testImage);
        Mockito.when(fileDBRepository.findAll()).thenReturn(expected);
        //act
        List<VehicleImage> actual = fileStorageService.getAllFiles().collect(Collectors.toList());
        //assert
        Assertions.assertEquals(expected, actual);

    }


}
