package project.service;

import project.exceptions.EntityNotFoundException;
import com.telerikacademy.models.*;
import project.models.enums.RequestStatus;
import project.repositories.common.PolicyRequestRepository;
import project.services.FileStorageServiceImpl;
import project.services.PolicyRequestServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import project.models.PolicyRequest;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PolicyRequestServiceImplTests {

    @Mock
    PolicyRequestRepository policyRequestRepository;

    @Mock
    FileStorageServiceImpl fileStorageServiceImpl;

    @InjectMocks
    PolicyRequestServiceImpl policyRequestService;

    private PolicyRequest testPolicy;
    private List<PolicyRequest> testPolicies;

    public static PolicyRequest getPolicy() {
        return new PolicyRequest();
    }

    @BeforeEach
    void init() {
        testPolicy = getPolicy();
        testPolicies = Collections.singletonList(testPolicy);
    }

    @Test
    public void get_ShouldReturn_WhenEntityExists() {
        //arrange
        Mockito.when(policyRequestRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(testPolicy));
        //act
        PolicyRequest actual = policyRequestService.getById(Mockito.anyInt());
        //assert
        Assertions.assertEquals(actual, testPolicy);
    }

    @Test
    public void get_ShouldThrow_WhenEntityDoesNotExist() {
        //arrange
        Mockito.when(policyRequestRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.empty());
        //act, assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> policyRequestService.getById(Mockito.anyInt()));
    }

    @Test
    public void create_ShouldCreate() throws IOException {

        MockMultipartFile img = new MockMultipartFile("img", "", "image/jpeg", "someImage".getBytes());
        //arrange, act
        policyRequestService.create(testPolicy, img);
        //assert
        Mockito.verify(policyRequestRepository, Mockito.times(1)).save(testPolicy);
    }

    @Test
    public void getAll_ShouldGetAllRequests() {
        Mockito.when(policyRequestRepository.findAll()).thenReturn(testPolicies);

        List<PolicyRequest> actual = policyRequestService.getAll();
        //act, assert
       Assertions.assertEquals(testPolicies, actual);
    }

    @Test
    public void approveRequestById_ShouldChangeRequestStatusToApproved() {
        PolicyRequest expectedResult = testPolicy;
        when(policyRequestRepository.findById(testPolicy.getId())).thenReturn(Optional.of(expectedResult));
        policyRequestService.approveRequestById(testPolicy.getId());
        verify(policyRequestRepository, times(1)).save(expectedResult);
        org.assertj.core.api.Assertions.assertThat(expectedResult.getRequestStatus()).isEqualTo(RequestStatus.APPROVED);
    }

    @Test
    public void declineRequestById_ShouldChangeRequestStatusToDeclined() {
        PolicyRequest expectedResult = testPolicy;
        when(policyRequestRepository.findById(testPolicy.getId())).thenReturn(Optional.of(expectedResult));
        policyRequestService.rejectRequestById(testPolicy.getId());
        verify(policyRequestRepository, times(1)).save(expectedResult);
        org.assertj.core.api.Assertions.assertThat(expectedResult.getRequestStatus()).isEqualTo(RequestStatus.REJECTED);
    }

    @Test
    public void cancelRequestById_ShouldChangeRequestStatusOfEntityWithSuchIdToCancelled(){
        PolicyRequest expectedResult = testPolicy;
        when(policyRequestRepository.findById(testPolicy.getId())).thenReturn(Optional.of(expectedResult));
        policyRequestService.cancelRequestById(testPolicy.getId());
        verify(policyRequestRepository, times(1)).save(expectedResult);
    }
}
