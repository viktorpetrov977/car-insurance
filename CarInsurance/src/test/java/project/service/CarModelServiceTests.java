package project.service;

import project.models.CarMake;
import project.models.CarModel;
import project.repositories.common.CarModelRepository;
import project.services.CarModelServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarModelServiceTests {
    @Mock
    CarModelRepository mockRepository;

    @InjectMocks
    CarModelServiceImpl carModelService;

    private CarModel testCarModel;
    private List<CarModel> testCarModels;
    private CarMake testCarMake;

    public static CarMake getCarMake() {
        return new CarMake(1, "Audi");
    }

    public static CarModel getCarModel() {
        CarMake carMake = new CarMake(1, "Audi");
        return new CarModel(1, carMake, "model");
    }

    @BeforeEach
    public void init() {
        testCarMake = getCarMake();
        testCarModel = getCarModel();
        testCarModels = Collections.singletonList(testCarModel);
    }

    @Test
    public void getAll_ShouldReturnCollection() {
        //arrange
        Mockito.when(mockRepository.findAll()).thenReturn(testCarModels);
        //act
        List<CarModel> actual = new ArrayList<>(carModelService.getAll());
        //assert
        Assertions.assertEquals(actual, testCarModels);
    }


    @Test
    public void getAllByCarBrand_ShouldReturnAllByCarBrand() {
        when(mockRepository.findAllByCarMake(testCarMake)).thenReturn(Collections.singletonList(testCarModel));
        List<CarModel> actualResult = carModelService.getAllModelsForBrand(testCarMake);
        Assertions.assertEquals(actualResult, testCarModels);
    }

}
