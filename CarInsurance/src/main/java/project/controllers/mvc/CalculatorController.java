package project.controllers.mvc;

import project.models.CarMake;
import project.models.CarModel;
import project.models.utils.MulticriteriaTableHelper;
import project.models.utils.PolicyCalculator;
import project.services.common.CarMakeService;
import project.services.common.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/calculator")
public class CalculatorController {
    private CarMakeService carMakeService;
    private CarModelService carModelService;
    private MulticriteriaTableHelper helper;

    @Autowired
    public CalculatorController(CarMakeService carMakeService, CarModelService carModelService, MulticriteriaTableHelper helper) {
        this.carMakeService = carMakeService;
        this.carModelService = carModelService;
        this.helper = helper;
        helper.getFromDatabase();
    }

    @GetMapping
    public String showCalculator(Model model, HttpSession session) {
        PolicyCalculator policyCalculatorResult = (PolicyCalculator) session.getAttribute("calculatorResult");
        // TODO: 11/3/2020 To refactor name of variable
        boolean isCreated = policyCalculatorResult == null;
        PolicyCalculator policyCalculator = new PolicyCalculator();
        if (!isCreated) {
            model.addAttribute("calculatorResult", policyCalculatorResult);
            policyCalculator = policyCalculatorResult;
            session.setAttribute("totalPremium", policyCalculator.calculateTotalPremium());
        }
        model.addAttribute("carBrands", carMakeService.getAll());
        model.addAttribute("calculator", policyCalculator);
        model.addAttribute("isCreated", isCreated);
        return "calculateOffer";
    }

    @PostMapping
    public String calculate(@Valid @ModelAttribute("calculator") PolicyCalculator calculator, BindingResult result, HttpSession session, Model model) {
        if(result.hasErrors()){
            model.addAttribute("hasErrors", true);
            model.addAttribute("carBrands", carMakeService.getAll());
            model.addAttribute("policy", new PolicyCalculator());
            return "calculateOffer";
        }
        try {
            calculator.getOffer().setTotalPremium(calculator.calculateTotalPremium());
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        session.setAttribute("calculatorResult", calculator);
        return "redirect:/calculator";
    }

    @GetMapping("/models")
    public
    @ResponseBody
    List<CarModel> findAllModels(@RequestParam(value = "carBrandId") int carBrandId) {
        CarMake carMake = carMakeService.getById(carBrandId);
        return carModelService.getAllModelsForBrand(carMake);
    }
}
