package project.controllers.mvc;

import project.models.utils.PolicyCalculator;
import project.services.common.CarMakeService;
import project.services.common.CarModelService;
import project.services.common.PolicyRequestService;
import project.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import project.models.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/offer")
public class OfferController {
    private UserService userService;
    private PolicyRequestService requestService;
    private CarMakeService carMakeService;
    private CarModelService carModelService;


    @Autowired
    public OfferController(UserService userService,
                           PolicyRequestService requestService,
                           CarMakeService carMakeService,
                           CarModelService carModelService) {
        this.userService = userService;
        this.requestService = requestService;
        this.carMakeService = carMakeService;
        this.carModelService = carModelService;
    }

    @GetMapping
    public String showOfferPage(Model model, HttpSession session){
        helper(session, model);
        model.addAttribute("policyrequest", new PolicyRequest());
        return "offer";
    }

    @PostMapping
    public String getRequest(@Valid @ModelAttribute("policyrequest") PolicyRequest request,
                             BindingResult result,
                             HttpSession session,
                             Model model,
                             Principal principal,
                             @RequestParam("image") MultipartFile img) throws IOException {
        if(result.hasErrors()){
            helper(session, model);
            return "offer";
        }

        User user = userService.getByUsernameFromUser(principal.getName());
        UserDetails userDetails = userService.getByUsername(user.getUsername());
        PolicyCalculator calculator = (PolicyCalculator) session.getAttribute("calculatorResult");
        Offer offer = calculator.getOffer();
        request.setUserDetails(userDetails);
        request.setOffer(offer);

        requestService.create(request, img);
        return "redirect:/requests";
    }

    // TODO: 11/3/2020 To refactor the name of the method 
    private void helper(HttpSession session, Model model) {
        PolicyCalculator policyCalculatorResult = (PolicyCalculator) session.getAttribute("calculatorResult");
        double totalPremium = (double) session.getAttribute("totalPremium");
        CarModel carModel = (CarModel) session.getAttribute("carModel");
        model.addAttribute("preOfferResult", policyCalculatorResult);
        model.addAttribute("totalPremium",totalPremium);
        model.addAttribute("carModel", carModel);
    }
}
