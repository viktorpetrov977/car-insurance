package project.controllers.mvc;

import project.models.PolicyRequest;
import project.models.User;
import project.models.UserDetails;
import project.models.enums.RequestStatus;
import project.models.utils.ImageUtils;
import project.services.common.PolicyRequestService;
import project.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/requests")
public class UserRequestsController {
    public static final String USER_NOT_AUTHORIZED = "User not authorized!";
    private PolicyRequestService policyRequestService;
    private final UserService userService;


    @Autowired
    public UserRequestsController(PolicyRequestService policyRequestService, UserService userService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
    }

    @GetMapping
    public String showRequests(Model model, Principal principal) {
        User user = userService.getByUsernameFromUser(principal.getName());
        UserDetails userDetails = userService.getByUsername(user.getUsername());
        model.addAttribute("user", userDetails);
        List<PolicyRequest> requests = policyRequestService.getAllByUser(userDetails);
        model.addAttribute("requests", requests);
        model.addAttribute("pending", RequestStatus.PENDING);
        model.addAttribute("imgUtil", new ImageUtils());
        return "requests";
    }

    @PostMapping("/{requestId}")
    public String cancelRequest(@PathVariable int requestId, Principal principal) {
        checkRequestOwner(requestId, principal);
        policyRequestService.cancelRequestById(requestId);
        return "redirect:/requests";
    }

    private void checkRequestOwner(int requestId, Principal principal) {
        User user = userService.getByUsernameFromUser(principal.getName());
        UserDetails userDetails = userService.getByUsername(user.getUsername());
        PolicyRequest policyRequest;
        try {
            policyRequest = policyRequestService.getById(requestId);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
        if (!policyRequest.getUserDetails().getUsername().equals(userDetails.getUsername())) {
            if (!user.isAgent()) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, USER_NOT_AUTHORIZED);
            }
        }
    }
}
