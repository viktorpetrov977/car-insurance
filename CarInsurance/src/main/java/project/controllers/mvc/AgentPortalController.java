package project.controllers.mvc;

import project.models.PolicyRequest;
import project.models.enums.RequestStatus;
import project.models.utils.ImageUtils;
import project.services.common.PolicyRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/agent_portal")
public class AgentPortalController {

    private PolicyRequestService policyRequestService;

    @Autowired
    public AgentPortalController(PolicyRequestService policyRequestService) {
        this.policyRequestService = policyRequestService;
    }

    @GetMapping
    public String agentRequests(Model model) {
        List<PolicyRequest> requests = policyRequestService.getAll();
        model.addAttribute("pending", RequestStatus.PENDING);
        model.addAttribute("requests", requests);
        model.addAttribute("imgUtil", new ImageUtils());

        return "agent_portal";
    }

    @PostMapping(value = "/{requestId}", params = "status=rejected")
    public String rejectRequest(@PathVariable int requestId) {
        policyRequestService.rejectRequestById(requestId);
        return "redirect:/agent_portal";
    }

    @PostMapping(value = "/{requestId}", params = "status=approved")
    public String approveRequest(@PathVariable int requestId) {
        policyRequestService.approveRequestById(requestId);
        return "redirect:/agent_portal";
    }
}
