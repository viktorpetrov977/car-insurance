package project.controllers.mvc;

import project.exceptions.UserAlreadyExistException;
import project.models.ConfirmationToken;
import project.models.User;
import project.models.dto.UserRegistrationDTO;
import project.models.utils.OnRegistrationCompleteEvent;
import project.repositories.common.ConfirmationTokenRepository;
import project.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private ConfirmationTokenRepository confirmationTokenRepository;
    private ApplicationEventPublisher eventPublisher;
    private MessageSource messages;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  UserService userService,
                                  ApplicationEventPublisher eventPublisher,
                                  @Qualifier("messageSource") MessageSource messages) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.eventPublisher = eventPublisher;
        this.messages = messages;
    }

    @GetMapping("/user/registration")
    public String showRegistrationForm(Model model) {
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
        model.addAttribute("user", userRegistrationDTO);
        return "registration";
    }

    @PostMapping("/user/registration")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user")  @Valid UserRegistrationDTO user, BindingResult bindingResult,
            HttpServletRequest request) {
        ModelAndView mav;
        if (bindingResult.hasErrors()) {
            return new ModelAndView("registration", bindingResult.getModel());
        }

        try {
            User registered = userService.registerNewUserAccount(user);

            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered,
                    request.getLocale(), appUrl));
        } catch (UserAlreadyExistException ex) {
            mav = new ModelAndView("registration", "user", user);
            mav.addObject("message", "An account for that username/email already exists.");
            return mav;
        }
        return new ModelAndView("successfulregistration", "user", user);
    }

    @GetMapping("/register-confirmation")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {

        Locale locale = request.getLocale();

        ConfirmationToken confirmationToken = userService.getConfirmationToken(token);
        if (confirmationToken == null) {
            String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/bad-user?lang=" + locale.getLanguage();
        }

        User user = confirmationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((confirmationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = messages.getMessage("auth.message.expired", null, locale);
            model.addAttribute("message", messageValue);
            return "redirect:/bad-user?lang=" + locale.getLanguage();
        }

        user.setEnabled(true);
        userService.saveRegisteredUser(user);
        return "index";
    }
}
