package project.controllers.mvc;

import project.exceptions.EntityNotFoundException;
import project.models.MulticriteriaTable;
import project.services.common.MulticriteriaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
@RequestMapping("/multicriteria")
public class MulticriteriaTableController {

    private MulticriteriaTableService multicriteriaTableService;

    @Autowired
    public MulticriteriaTableController(MulticriteriaTableService multicriteriaTableService) {
        this.multicriteriaTableService = multicriteriaTableService;
    }


    @GetMapping("/update")
    public String getAllTable(Model model){
       model.addAttribute("table", multicriteriaTableService.getAll());
       return "agent_portal";
    }

    @GetMapping("/update/{id}")
    public String getTable(Model model,@PathVariable int id) {
        try {
            model.addAttribute("table", multicriteriaTableService.getById(id));
            return "table";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/update/{id}")
    public String updateTable(
            @PathVariable int id,
            @Valid @ModelAttribute MulticriteriaTable multicriteriaTable,
            Model model
    ) {
        try {
            MulticriteriaTable multicriteriaTableToUpdate = multicriteriaTableService.getById(id);
            multicriteriaTableToUpdate.setCcMin(multicriteriaTable.getCcMin());
            multicriteriaTableToUpdate.setCcMax(multicriteriaTable.getCcMax());
            multicriteriaTableToUpdate.setCarAgeMin(multicriteriaTable.getCarAgeMin());
            multicriteriaTableToUpdate.setCarAgeMax(multicriteriaTable.getCarAgeMax());
            multicriteriaTableToUpdate.setBaseAmount(multicriteriaTable.getBaseAmount());
            multicriteriaTableService.updateTable(multicriteriaTable);

            return "redirect:/";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
