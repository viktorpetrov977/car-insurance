package project.controllers.mvc;


import project.exceptions.DuplicateEntityException;
import project.models.User;
import project.models.UserDetails;
import project.models.utils.ImageUtils;
import project.services.common.PolicyRequestService;
import project.services.common.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {
    private UserService userService;
    private PolicyRequestService policyRequestService;


    public UserController(UserService userService,
                          PolicyRequestService policyRequestService) {
        this.userService = userService;
        this.policyRequestService = policyRequestService;
    }

    @GetMapping("/profile")
    public String showUserProfile(Model model,
                                  Principal principal
    ) {
        UserDetails user = userService.getByUsername(principal.getName());

        model.addAttribute("imgUtil", new ImageUtils());
        model.addAttribute("user", user);
        return "user-profile";
    }

    @GetMapping("/edit_user/{username}")
    public String showEditUserPage(Model model,
                                   @PathVariable String username)
                                   {
        User user = userService.getByUsernameFromUser(username);
        UserDetails userDetails = userService.getByUsername(user.getUsername());

        userDetails.setUsername(user.getUsername());

        model.addAttribute("userToUpdateDetails", userDetails);
        model.addAttribute("user", user);
        return "edit_user";
    }

    @PostMapping("/edit_user/{username}")
    public String editUser(@PathVariable("username") String username,
                           @ModelAttribute("userToUpdateDetails") UserDetails userToUpdateDetails,
                           Principal principal, BindingResult bindingResult, @RequestParam("image") MultipartFile img) {
        try {
            if (bindingResult.hasErrors()) {
                return "user-profile";
            }
            UserDetails userDetails = userService.getByUsername(principal.getName());
            userDetails.setFirstName(userToUpdateDetails.getFirstName());
            userDetails.setLastName(userToUpdateDetails.getLastName());
            userDetails.setPhoneNumber(userToUpdateDetails.getPhoneNumber());
            userDetails.setAddress(userToUpdateDetails.getAddress());
            userDetails.setBirthDate(userToUpdateDetails.getBirthDate());
            userService.update(userDetails, img);
            return "redirect:/user/profile";
        } catch (DuplicateEntityException | IOException e) {
            bindingResult.rejectValue("username", e.getMessage());
            return "user-profile";
        }
    }

}
