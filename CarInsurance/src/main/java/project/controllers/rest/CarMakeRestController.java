package project.controllers.rest;

import project.models.CarMake;
import project.services.common.CarMakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/makes")
public class CarMakeRestController {

    private CarMakeService carMakeService;

    @Autowired
    public CarMakeRestController(CarMakeService carMakeService) {
        this.carMakeService = carMakeService;
    }

    @GetMapping
    List<CarMake> getAll() {
        return carMakeService.getAll();
    }

    @GetMapping({"id"})
    CarMake getById(@PathVariable int id) {
        return carMakeService.getById(id);
    }
 }
