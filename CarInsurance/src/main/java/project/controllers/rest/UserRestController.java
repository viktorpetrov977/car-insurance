package project.controllers.rest;

import project.exceptions.DuplicateEntityException;
import project.exceptions.EntityNotFoundException;
import project.mappers.UserMapper;
import project.models.UserDetails;
import project.models.dto.UserRegistrationDTO;
import project.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")

public class UserRestController {

    public static final String PASSWORD_DOES_NOT_MATCH = "Password does not match! Please try again!";
    public static final String USER_ALREADY_EXISTS = "User with this username/email already exists!";
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsManager userDetailsManager;

    @Autowired
    public UserRestController(UserService userService, PasswordEncoder passwordEncoder, UserDetailsManager userDetailsManager) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
    }

    @GetMapping
    public List<UserDetails> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserDetails create(@Valid @RequestBody UserRegistrationDTO userRegistrationDTO) {
        try {
            if (!userRegistrationDTO.getPassword().equals(userRegistrationDTO.getPasswordConfirmation())) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, PASSWORD_DOES_NOT_MATCH);
            }
            createUserInUserTable(userRegistrationDTO);
            UserDetails userDetails = UserMapper.fromUserDtoToUser(userRegistrationDTO);
            userService.create(userDetails);
            return userDetails;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            userService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    private void createUserInUserTable(UserRegistrationDTO userRegistrationDTO) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User
                        (userRegistrationDTO.getUsername(), passwordEncoder.encode(userRegistrationDTO.getPassword()), authorities);

        if (userDetailsManager.userExists(userRegistrationDTO.getUsername())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, USER_ALREADY_EXISTS);
        }
        userDetailsManager.createUser(newUser);
    }
}






