package project.controllers.rest;

import project.models.CarModel;
import project.services.common.CarMakeService;
import project.services.common.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/models")
public class CarModelRestController {
    private CarModelService carModelService;
    private CarMakeService carMakeService;

    @Autowired
    public CarModelRestController(CarModelService carModelService, CarMakeService carMakeService) {
        this.carModelService = carModelService;
        this.carMakeService = carMakeService;
    }

    @GetMapping
    List<CarModel> getAllModels() {
        return carModelService.getAll();
    }

    @GetMapping("{id}")
    CarModel getById(@PathVariable int id) {
        return carModelService.getById(id);
    }
}
