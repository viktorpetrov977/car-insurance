package project.models;

import project.models.enums.RequestStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import java.time.LocalDate;

@Entity
@Table(name = "policy_request")
public class PolicyRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "effective_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate effectiveDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetails userDetails;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private RequestStatus requestStatus = RequestStatus.PENDING;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "carModel", column = @Column(name = "carModel_id")),
            @AttributeOverride(name = "cubicCapacity", column = @Column(name = "cubic_capacity")),
            @AttributeOverride(name = "firstRegistrationDate", column = @Column(name = "first_registration_date")),
            @AttributeOverride(name = "driverAge", column = @Column(name = "driver_age")),
            @AttributeOverride(name = "accidentPrevYear", column = @Column(name = "accident_prev_year"))
    })
    @Valid
    private Offer offer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "vehicle_image_id")
    private VehicleImage vehicleImage;

    public PolicyRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public VehicleImage getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(VehicleImage vehicleImage) {
        this.vehicleImage = vehicleImage;
    }
}
