package project.models.utils;

import project.models.MulticriteriaTable;
import project.models.Offer;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
@Component
@Scope("session")
public class PolicyCalculator {
    // TODO: 11/3/2020 Refactor this and make service as well
    
    @Valid
    private Offer offer;

    private final List<MulticriteriaTable> multicriteriaTable;

    public PolicyCalculator(List<MulticriteriaTable> multicriteriaTable) {
        this.multicriteriaTable = multicriteriaTable;
    }

    public PolicyCalculator() {
        multicriteriaTable = MulticriteriaTableHelper.getMulticriteriaTable();
    }


    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    private double calculateBaseAmount() {
        double baseAmount = 0;
        LocalDate today = LocalDate.now();
        int carAge = Period.between(offer.getFirstRegistrationDate(), today).getYears();

        for (int i = 0; i < multicriteriaTable.size() - 1; i++) {
            if (offer.getCubicCapacity() >= multicriteriaTable.get(i).getCcMin() && offer.getCubicCapacity() <= multicriteriaTable.get(i).getCcMax()) {
                if (carAge >= multicriteriaTable.get(i).getCarAgeMin() && carAge <= multicriteriaTable.get(i).getCarAgeMax()) {
                    baseAmount = multicriteriaTable.get(i).getBaseAmount();
                } else {
                    baseAmount = multicriteriaTable.get(i + 1).getBaseAmount();
                }
                break;
            }
        }
        return baseAmount;
    }

    private double calculateNetPremium() {
        double baseAmount;
        baseAmount = calculateBaseAmount();

        double netPremium = baseAmount;
        if (offer.isAccidentPrevYear()) {
            netPremium += baseAmount * 0.2;
        }

        if (offer.getDriverAge() < 25) {
            netPremium += baseAmount * 0.05;
        }
        return Math.ceil(netPremium);
    }

    public double calculateTotalPremium() {
        double netPremium = calculateNetPremium();
        return Math.ceil(netPremium * 0.1) + netPremium;
    }

    public List<MulticriteriaTable> getMulticriteriaTable() {
        return multicriteriaTable;
    }
}