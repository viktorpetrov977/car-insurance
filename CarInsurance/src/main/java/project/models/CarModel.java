package project.models;

import javax.persistence.*;

@Entity
@Table(name = "car_model")
@Embeddable
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "carMake_id")
    private CarMake carMake;

    @Column(name = "name")
    private String name;

    public CarModel() {
    }

    public CarModel(int id, CarMake carMake, String name) {
        this.id = id;
        this.carMake = carMake;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarMake getCarMake() {
        return carMake;
    }

    public void setCarMake(CarMake carMake) {
        this.carMake = carMake;
    }
}
