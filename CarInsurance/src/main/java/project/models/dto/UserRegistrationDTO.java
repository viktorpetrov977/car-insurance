package project.models.dto;

import project.validations.PasswordMatches;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@PasswordMatches
public class UserRegistrationDTO {
    private static final String EMAIL_REGEX = "^(.+)@(.+)$";
    public static final String PASSWORD_MUST_BE_AT_LEAST_6_CHARACTERS = "Password must be at least 6 characters!";

    @NotEmpty(message = "This field cannot be blank!")
    @Email(regexp = EMAIL_REGEX)
    private String username;

    @NotEmpty
    @Size(min = 5, message = "Min 5 characters!")
    private String firstName;

    @NotEmpty
    @Size(min = 5, message = "Min 5 characters!")
    private String lastName;

    @NotEmpty
    @Size(min = 6, message = PASSWORD_MUST_BE_AT_LEAST_6_CHARACTERS)
    private String password;

    @NotEmpty
    private String passwordConfirmation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
