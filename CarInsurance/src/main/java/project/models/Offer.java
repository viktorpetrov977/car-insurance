package project.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Embeddable
public class Offer {

    @ManyToOne(cascade = CascadeType.MERGE)
    private CarModel carModel;

    private boolean accidentPrevYear;

    @Min(18)
    @Max(99)
    private int driverAge;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    // TODO: 11/1/2020 Validation date must be max NOW
    private LocalDate firstRegistrationDate;

    private double totalPremium;

    @Min(100)
    @Max(10000)
    private int cubicCapacity;

    public Offer() {
    }

    public boolean isAccidentPrevYear() {
        return accidentPrevYear;
    }

    public void setAccidentPrevYear(boolean accidentPrevYear) {
        this.accidentPrevYear = accidentPrevYear;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public LocalDate getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(LocalDate firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }
}
