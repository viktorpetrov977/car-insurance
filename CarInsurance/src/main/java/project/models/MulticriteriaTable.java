package project.models;

import javax.persistence.*;

@Entity
@Table(name = "multicriteria_table")
public class MulticriteriaTable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "base_amount")
    private double baseAmount;

    @Column(name = "car_age_min")
    private int carAgeMin;

    @Column(name = "car_age_max")
    private int carAgeMax;

    @Column(name = "cc_min")
    private int ccMin;

    @Column(name = "cc_max")
    private int ccMax;

    public MulticriteriaTable() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public void setCarAgeMin(int carAgeMin) {
        this.carAgeMin = carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public void setCarAgeMax(int carAgeMax) {
        this.carAgeMax = carAgeMax;
    }

    public int getCcMin() {
        return ccMin;
    }

    public void setCcMin(int ccMin) {
        this.ccMin = ccMin;
    }

    public int getCcMax() {
        return ccMax;
    }

    public void setCcMax(int ccMax) {
        this.ccMax = ccMax;
    }
}
