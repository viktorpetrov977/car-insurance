package project.models.enums;

public enum RequestStatus {
    PENDING("Pending"),
    REJECTED("Rejected"),
    APPROVED("Approved"),
    CANCELLED("Cancelled");

    private String viewName;

    RequestStatus(String viewName) {
        this.viewName = viewName;
    }

    public String getViewName() {
        return viewName;
    }
}
