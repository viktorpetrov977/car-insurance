package project.validations;

import project.models.enums.AllowedContent;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ValidFileImpl implements ConstraintValidator<ValidFile, MultipartFile> {
    private Set<String> allowedContentTypes;

    @Override
    public void initialize(ValidFile constraintAnnotation) {
        allowedContentTypes = Arrays.stream(constraintAnnotation.value()).map(AllowedContent::type).collect(Collectors.toSet());
    }

    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext context) {
            if (multipartFile == null) return false;
            boolean notEmpty = !multipartFile.isEmpty();
            boolean allowedContent = allowedContentTypes.contains(multipartFile.getContentType());
            return notEmpty && allowedContent;
    }
}
