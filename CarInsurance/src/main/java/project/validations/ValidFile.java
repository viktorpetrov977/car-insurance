package project.validations;

import project.models.enums.AllowedContent;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidFileImpl.class})
public @interface ValidFile {
    AllowedContent[] value() default {};

    String message() default "{safetycar.annotations.ValidFile.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
