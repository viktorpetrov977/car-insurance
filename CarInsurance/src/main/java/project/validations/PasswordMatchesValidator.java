package project.validations;

import project.models.dto.UserRegistrationDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    public void initialize(PasswordMatches constraint) {
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserRegistrationDTO userRegistrationDto = (UserRegistrationDTO) obj;
        return userRegistrationDto.getPassword().equals(userRegistrationDto.getPasswordConfirmation());
    }
}
