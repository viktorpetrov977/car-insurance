package project.services;

import project.exceptions.EntityNotFoundException;
import project.models.PolicyRequest;
import project.models.UserDetails;
import project.models.VehicleImage;
import project.models.enums.RequestStatus;
import project.repositories.common.PolicyRequestRepository;
import project.services.common.PolicyRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class PolicyRequestServiceImpl implements PolicyRequestService {
    public static final String POLICY_REQUEST_NOT_FOUND = "Policy request with id %d not found!";
    private PolicyRequestRepository policyRequestRepository;
    private FileStorageServiceImpl storageService;

    @Autowired
    public PolicyRequestServiceImpl(PolicyRequestRepository policyRequestRepository, FileStorageServiceImpl storageService) {
        this.policyRequestRepository = policyRequestRepository;
        this.storageService = storageService;
    }

    @Override
    public List<PolicyRequest> getAll() {
        return policyRequestRepository.findAll();
    }

    @Override
    public List<PolicyRequest> getAllByUser(UserDetails userDetails) {
        return policyRequestRepository.getAllByUserDetails(userDetails);
    }

    @Override
    public PolicyRequest getById(int id) {
        return policyRequestRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format(POLICY_REQUEST_NOT_FOUND, id)));
    }

    @Override
    public PolicyRequest create(PolicyRequest policyRequest, MultipartFile img) throws IOException {
        VehicleImage vehicleImage = storageService.store(img);
        policyRequest.setVehicleImage(vehicleImage);
        return policyRequestRepository.save(policyRequest);
    }

    // TODO: 11/3/2020 Refactor these methods 

    @Override
    public void approveRequestById(int id) {
        PolicyRequest policyRequest = getById(id);
        policyRequest.setRequestStatus(RequestStatus.APPROVED);
        policyRequestRepository.save(policyRequest);
    }

    @Override
    public void rejectRequestById(int id) {
        PolicyRequest policyRequest = getById(id);
        policyRequest.setRequestStatus(RequestStatus.REJECTED);
        policyRequestRepository.save(policyRequest);
    }

    @Override
    public void cancelRequestById(int id) {
        PolicyRequest policyRequest = getById(id);
        policyRequest.setRequestStatus(RequestStatus.CANCELLED);
        policyRequestRepository.save(policyRequest);
    }
}
