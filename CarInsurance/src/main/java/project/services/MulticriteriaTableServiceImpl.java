package project.services;

import project.exceptions.EntityNotFoundException;
import project.models.MulticriteriaTable;
import project.repositories.common.MulticriteriaTableRepository;
import project.services.common.MulticriteriaTableService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class MulticriteriaTableServiceImpl implements MulticriteriaTableService {

    private final MulticriteriaTableRepository multicriteriaTableRepository;

    @Autowired
    public MulticriteriaTableServiceImpl(MulticriteriaTableRepository multicriteriaTableRepository) {
        this.multicriteriaTableRepository = multicriteriaTableRepository;
    }


    @Override
    public void getTable() {

    }

    @Override
    public List<MulticriteriaTable> getAll() {
        List<MulticriteriaTable> result = new ArrayList<>();
        CollectionUtils.addAll(result, multicriteriaTableRepository.findAll());
        return result;
    }

    @Override
    public MulticriteriaTable getById(int id) {
        return multicriteriaTableRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Not found!"));
    }

//    @Override

//    public MulticriteriaTable getById(int id) {
//        List<MulticriteriaTable> result = new ArrayList<>();
//        CollectionUtils.addAll(result, multicriteriaTableRepository.findAll());
//        MulticriteriaTable mctRow = result.get(0);
//        return mctRow;
//    }

    @Transactional
    @Override
    public MulticriteriaTable updateTable(MulticriteriaTable multicriteriaTable) {
        multicriteriaTableRepository.save(multicriteriaTable);
        return multicriteriaTable;
    }
}
