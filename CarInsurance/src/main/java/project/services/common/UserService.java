package project.services.common;

import project.models.ConfirmationToken;
import project.models.UserDetails;
import project.models.User;
import project.models.dto.UserRegistrationDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface UserService{

    void create(UserDetails userDetails);

    UserDetails getById(int id);

    UserDetails getByUsername(String username);

    List<UserDetails> getAll();

    void update(UserDetails userDetails, MultipartFile img) throws IOException;

    void delete(int id);

    User getByUsernameFromUser(String username);

    void update(User user);

    void createConfirmationToken(User user, String token);

    ConfirmationToken getConfirmationToken(String confirmationToken);

    User getUser(String verificationToken);

    User registerNewUserAccount(UserRegistrationDTO userDto);

    void saveRegisteredUser(User user);
}
