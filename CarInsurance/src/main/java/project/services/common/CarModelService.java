package project.services.common;

import project.models.CarMake;
import project.models.CarModel;

import java.util.List;

public interface CarModelService {
    List<CarModel> getAll();
    CarModel getById(int id);
    List<CarModel> getAllModelsForBrand(CarMake carMake);
}
