package project.services.common;

import project.models.PolicyRequest;
import project.models.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PolicyRequestService{
    List<PolicyRequest> getAll();

    PolicyRequest getById(int id);

    PolicyRequest create(PolicyRequest policyRequest, MultipartFile img) throws IOException;

    List<PolicyRequest> getAllByUser(UserDetails userDetails);

    void approveRequestById(int id);

    void rejectRequestById(int id);

    void cancelRequestById(int id);
}
