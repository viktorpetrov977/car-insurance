package project.services.common;

import project.models.CarMake;

import java.util.List;

public interface CarMakeService {
    List<CarMake> getAll();
    CarMake getById(int id);
}
