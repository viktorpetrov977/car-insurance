package project.services.common;

import project.models.MulticriteriaTable;

import java.util.List;

public interface MulticriteriaTableService {
    void getTable();

    List<MulticriteriaTable> getAll();

    MulticriteriaTable getById(int id);

    MulticriteriaTable updateTable(MulticriteriaTable multicriteriaTable);
}
