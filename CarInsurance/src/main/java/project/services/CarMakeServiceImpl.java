package project.services;

import project.models.CarMake;
import project.repositories.common.CarMakeRepository;
import project.services.common.CarMakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarMakeServiceImpl implements CarMakeService {
    private CarMakeRepository carMakeRepository;

    @Autowired
    public CarMakeServiceImpl(CarMakeRepository carMakeRepository) {
        this.carMakeRepository = carMakeRepository;
    }

    @Override
    public List<CarMake> getAll() {
        return carMakeRepository.findAll();
    }

    @Override
    public CarMake getById(int id) {
        return carMakeRepository.getOne(id);
    }
}
