package project.services;

import project.models.VehicleImage;
import project.repositories.common.FileDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class FileStorageServiceImpl {

    private final FileDBRepository fileDBRepository;

    @Autowired
    public FileStorageServiceImpl(FileDBRepository fileDBRepository) {
        this.fileDBRepository = fileDBRepository;
    }

    public VehicleImage store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        VehicleImage VehicleImage = new VehicleImage(fileName, file.getContentType(), file.getBytes());

        return fileDBRepository.save(VehicleImage);
    }

    public VehicleImage getFile(Integer id) {
        return fileDBRepository.findById(id).get();
    }

    public Stream<VehicleImage> getAllFiles() {
        return fileDBRepository.findAll().stream();
    }
}
