package project.services;

import project.models.CarMake;
import project.models.CarModel;
import project.repositories.common.CarModelRepository;
import project.services.common.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {
    private final CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    @Override
    public List<CarModel> getAll() {
        return carModelRepository.findAll();
    }

    @Override
    public CarModel getById(int id) {
        return carModelRepository.getOne(id);
    }

    @Override
    public List<CarModel> getAllModelsForBrand(CarMake carMake) {
        return carModelRepository.findAllByCarMake(carMake);
    }
}

