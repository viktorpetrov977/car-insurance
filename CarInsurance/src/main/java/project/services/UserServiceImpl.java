package project.services;

import project.exceptions.DuplicateEntityException;
import project.exceptions.EntityNotFoundException;
import project.mappers.UserMapper;
import project.models.UserImage;
import project.models.dto.UserRegistrationDTO;
import project.repositories.common.ConfirmationTokenRepository;
import project.repositories.common.UserRepository;
import project.services.common.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import project.models.ConfirmationToken;
import project.models.User;
import project.models.UserDetails;

import java.io.IOException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static final String DUPLICATE_USERNAME_MESSAGE = "There is already an account with %s username/email!";
    private static final String USER_NOT_ACTIVE = "User with username/email %s not active!";
    private UserRepository userRepository;

    private ConfirmationTokenRepository tokenRepository;

    private JavaMailSender mailSender;

    private UserDetailsManager userDetailsManager;

    private PasswordEncoder passwordEncoder;

    private UserImageStorageService userImageStorageService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ConfirmationTokenRepository tokenRepository, JavaMailSender mailSender, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, UserImageStorageService userImageStorageService) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.mailSender = mailSender;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userImageStorageService = userImageStorageService;
    }

    @Override
    public void create(UserDetails userDetails) {
        validateUniqueUsername(userDetails.getUsername());
        userRepository.create(userDetails);
    }

    @Override
    public UserDetails getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public UserDetails getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public List<UserDetails> getAll() {
        return userRepository.getAll();
    }

    @Override
    public void update(UserDetails userDetails, MultipartFile img) throws IOException {
        UserImage userImage = userImageStorageService.store(img);
        userDetails.setUserImage(userImage);
        userRepository.update(userDetails);
    }

    @Override
    public void delete(int id) {
        userRepository.delete(id);
    }

    @Override
    public User getByUsernameFromUser(String username) {
        User user = userRepository.getByUsernameFromUser(username);
        if (!user.isEnabled()) {
            throw new EntityNotFoundException(String.format(USER_NOT_ACTIVE, username));
        }
        return user;
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public void createConfirmationToken(User user, String token) {
        ConfirmationToken newToken = new ConfirmationToken(token, user);
        tokenRepository.save(newToken);
    }

    @Override
    public ConfirmationToken getConfirmationToken(String confirmationToken) {
        return tokenRepository.findByConfirmationToken(confirmationToken);
    }

    @Override
    public User getUser(String confirmationToken) {
        return tokenRepository.findByConfirmationToken(confirmationToken).getUser();
    }

    @Override
    public User registerNewUserAccount(UserRegistrationDTO userDto) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getUsername(), passwordEncoder.encode(userDto.getPassword()),
                        false, true, true, true, authorities);
        userDetailsManager.createUser(newUser);
        User user = userRepository.getByUsernameFromUser(userDto.getUsername());
        UserDetails userDetails = UserMapper.fromUserDtoToUser(userDto);
        userRepository.create(userDetails);
        user.setUserDetails(userDetails);
        userRepository.update(userDetails);
        return user;
    }

    @Override
    public void saveRegisteredUser(User user) {
        userRepository.saveUser(user);
    }

    private void validateUniqueUsername(String name) {
        if (userRepository.existsByName(name)) {
            throw new DuplicateEntityException(
                    String.format(DUPLICATE_USERNAME_MESSAGE, name));
        }
    }


}
