package project.services;

import project.models.UserImage;
import project.repositories.common.UserImageDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class UserImageStorageService {
    private final UserImageDBRepository userImageDBRepository;

    @Autowired
    public UserImageStorageService(UserImageDBRepository userImageDBRepository) {
        this.userImageDBRepository = userImageDBRepository;
    }

    public UserImage store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        UserImage userImage = new UserImage(fileName, file.getContentType(), file.getBytes());

        return userImageDBRepository.save(userImage);
    }

    public UserImage getFile(Integer id) {
        return userImageDBRepository.findById(id).get();
    }

    public Stream<UserImage> getAllFiles() {
        return userImageDBRepository.findAll().stream();
    }
}
