package project.repositories.common;
import project.models.VehicleImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDBRepository extends JpaRepository <VehicleImage, Integer> {
}
