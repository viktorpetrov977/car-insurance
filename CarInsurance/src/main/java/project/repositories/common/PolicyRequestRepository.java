package project.repositories.common;

import project.models.PolicyRequest;
import project.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolicyRequestRepository extends JpaRepository<PolicyRequest, Integer> {
    List<PolicyRequest> getAllByUserDetails(UserDetails userDetails);
}
