package project.repositories.common;

import project.models.MulticriteriaTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MulticriteriaTableRepository extends JpaRepository<MulticriteriaTable, Integer> {

}
