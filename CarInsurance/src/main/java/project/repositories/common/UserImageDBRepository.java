package project.repositories.common;
import project.models.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageDBRepository extends JpaRepository <UserImage, Integer> {
}
