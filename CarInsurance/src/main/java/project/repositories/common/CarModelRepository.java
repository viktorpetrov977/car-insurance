package project.repositories.common;

import project.models.CarMake;
import project.models.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel, Integer> {
    List<CarModel> findAllByCarMake(CarMake carMake);
}
