package project.repositories.common;

import project.models.User;
import project.models.UserDetails;

import java.util.List;


public interface UserRepository {
    void create(UserDetails userDetails);

    UserDetails getById(int id);

    User getUserById(int id);

    UserDetails getByUsername(String username);

    User getByUsernameFromUser(String username);

    boolean existsByName(String name);

    List<UserDetails> getAll();

    void update(UserDetails userDetails);

    void delete(int id);

    void update(User user);

    void enable(String userName);

    void disable(String userName);

    void saveUser(User user);
}
