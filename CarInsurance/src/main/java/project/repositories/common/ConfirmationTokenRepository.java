package project.repositories.common;

import project.models.ConfirmationToken;
import project.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Integer> {
    ConfirmationToken findByConfirmationToken(String token);
    ConfirmationToken findByUser(User user);
}
