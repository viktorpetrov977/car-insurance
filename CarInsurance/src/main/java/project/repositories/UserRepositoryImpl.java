package project.repositories;

import project.exceptions.EntityNotFoundException;
import project.models.User;
import project.repositories.common.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import project.models.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    public static final String USER_WITH_ID_NOT_FOUND = "User with id %d not found!";
    public static final String USER_WITH_USERNAME_NOT_FOUND = "User with username %s not found";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userDetails);
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails userDetails = session.get(UserDetails.class, id);
            if (userDetails == null) {
                throw new EntityNotFoundException(
                        String.format(USER_WITH_ID_NOT_FOUND, id));
            }
            return userDetails;
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(
                        String.format(USER_WITH_ID_NOT_FOUND, id));
            }
            return user;
        }
    }

    @Override
    public UserDetails getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where username = :username", UserDetails.class);
            query.setParameter("username", username);
            List<UserDetails> userDetails = query.list();
            if (userDetails.isEmpty()) {
                throw new EntityNotFoundException(String.format(USER_WITH_USERNAME_NOT_FOUND, username));
            }
            return userDetails.get(0);
        }
    }

    @Override
    public User getByUsernameFromUser(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(String.format(USER_WITH_USERNAME_NOT_FOUND, username));
            }
            return users.get(0);
        }
    }

    @Override // TODO: 11.10.2020 г. check this out!
    public boolean existsByName(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where username = :username", UserDetails.class);
            query.setParameter("username", username);
            List<UserDetails> userDetails = query.list();
            if (userDetails.isEmpty()) {
                return false;
            }
            return true;
        }
    }

    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails ", UserDetails.class);
            return query.list();
        }
    }

    @Override
    public void update(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(getById(id));
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void enable(String userName) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(getByUsernameFromUser(userName));
            session.getTransaction().commit();
        }
    }

    @Override
    public void disable(String userName) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(getByUsernameFromUser(userName));
            session.getTransaction().commit();
        }
    }

    @Override
    public void saveUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }
}
