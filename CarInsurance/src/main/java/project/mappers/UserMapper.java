package project.mappers;

import project.models.UserDetails;
import project.models.dto.UserRegistrationDTO;

public class UserMapper {
    public static UserDetails fromUserDtoToUser(UserRegistrationDTO userRegistrationDTO) {
        UserDetails userDetails = new UserDetails();
        userDetails.setUsername(userRegistrationDTO.getUsername());
        userDetails.setFirstName(userRegistrationDTO.getFirstName());
        userDetails.setLastName(userRegistrationDTO.getLastName());

        return userDetails;
    }
}
