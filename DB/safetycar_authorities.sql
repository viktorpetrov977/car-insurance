create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.authorities (username, authority) VALUES ('testUser@test.com', 'ROLE_USER');
INSERT INTO safetycar.authorities (username, authority) VALUES ('testUser@test.com', 'ROLE_AGENT');
INSERT INTO safetycar.authorities (username, authority) VALUES ('testUser@test.com', 'ROLE_ADMIN');
INSERT INTO safetycar.authorities (username, authority) VALUES ('newtestUser@test.com', 'ROLE_USER');
INSERT INTO safetycar.authorities (username, authority) VALUES ('viktorpetrov977@gmail.com', 'ROLE_USER');