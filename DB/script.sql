create table car_make
(
    id   int          not null
        primary key,
    name varchar(255) not null
);

INSERT INTO safetycar.car_make (id, name) VALUES (0, 'Alfa Romeo');
INSERT INTO safetycar.car_make (id, name) VALUES (1, 'Aston Martin');
INSERT INTO safetycar.car_make (id, name) VALUES (2, 'Audi');
INSERT INTO safetycar.car_make (id, name) VALUES (3, 'Bentley');
INSERT INTO safetycar.car_make (id, name) VALUES (4, 'BMW');
INSERT INTO safetycar.car_make (id, name) VALUES (5, 'Cadillac');
INSERT INTO safetycar.car_make (id, name) VALUES (6, 'Chery');
INSERT INTO safetycar.car_make (id, name) VALUES (7, 'Chevrolet');
INSERT INTO safetycar.car_make (id, name) VALUES (8, 'Chrysler');
INSERT INTO safetycar.car_make (id, name) VALUES (9, 'Citroen');
INSERT INTO safetycar.car_make (id, name) VALUES (10, 'Dacia');
INSERT INTO safetycar.car_make (id, name) VALUES (11, 'Daewoo');
INSERT INTO safetycar.car_make (id, name) VALUES (12, 'Daihatsu');
INSERT INTO safetycar.car_make (id, name) VALUES (13, 'DFM');
INSERT INTO safetycar.car_make (id, name) VALUES (14, 'Dodge');
INSERT INTO safetycar.car_make (id, name) VALUES (15, 'Ferrari');
INSERT INTO safetycar.car_make (id, name) VALUES (16, 'Fiat');
INSERT INTO safetycar.car_make (id, name) VALUES (17, 'Ford');
INSERT INTO safetycar.car_make (id, name) VALUES (18, 'Geely');
INSERT INTO safetycar.car_make (id, name) VALUES (19, 'Honda');
INSERT INTO safetycar.car_make (id, name) VALUES (20, 'Hyundai');
INSERT INTO safetycar.car_make (id, name) VALUES (21, 'Infiniti');
INSERT INTO safetycar.car_make (id, name) VALUES (22, 'Isuzu');
INSERT INTO safetycar.car_make (id, name) VALUES (23, 'Jaguar');
INSERT INTO safetycar.car_make (id, name) VALUES (24, 'Jeep');
INSERT INTO safetycar.car_make (id, name) VALUES (25, 'Kia');
INSERT INTO safetycar.car_make (id, name) VALUES (26, 'Lada');
INSERT INTO safetycar.car_make (id, name) VALUES (27, 'Lamborghini');
INSERT INTO safetycar.car_make (id, name) VALUES (28, 'Lancia');
INSERT INTO safetycar.car_make (id, name) VALUES (29, 'Land Rover');
INSERT INTO safetycar.car_make (id, name) VALUES (30, 'Maserati');
INSERT INTO safetycar.car_make (id, name) VALUES (31, 'Mazda');
INSERT INTO safetycar.car_make (id, name) VALUES (32, 'Mercedes');
INSERT INTO safetycar.car_make (id, name) VALUES (33, 'Mini');
INSERT INTO safetycar.car_make (id, name) VALUES (34, 'Mitsubishi');
INSERT INTO safetycar.car_make (id, name) VALUES (35, 'Nissan');
INSERT INTO safetycar.car_make (id, name) VALUES (36, 'Opel');
INSERT INTO safetycar.car_make (id, name) VALUES (37, 'Peugeot');
INSERT INTO safetycar.car_make (id, name) VALUES (38, 'Porsche');
INSERT INTO safetycar.car_make (id, name) VALUES (39, 'Proton');
INSERT INTO safetycar.car_make (id, name) VALUES (40, 'Renault');
INSERT INTO safetycar.car_make (id, name) VALUES (41, 'Rover');
INSERT INTO safetycar.car_make (id, name) VALUES (42, 'Saab');
INSERT INTO safetycar.car_make (id, name) VALUES (43, 'Seat');
INSERT INTO safetycar.car_make (id, name) VALUES (44, 'Skoda');
INSERT INTO safetycar.car_make (id, name) VALUES (45, 'Smart');
INSERT INTO safetycar.car_make (id, name) VALUES (46, 'SsangYong');
INSERT INTO safetycar.car_make (id, name) VALUES (47, 'Subaru');
INSERT INTO safetycar.car_make (id, name) VALUES (48, 'Suzuki');
INSERT INTO safetycar.car_make (id, name) VALUES (49, 'Tata');
INSERT INTO safetycar.car_make (id, name) VALUES (50, 'TofaE');
INSERT INTO safetycar.car_make (id, name) VALUES (51, 'Toyota');
INSERT INTO safetycar.car_make (id, name) VALUES (52, 'Volkswagen');
INSERT INTO safetycar.car_make (id, name) VALUES (53, 'Volvo');

create table car_model
(
    id         int          not null
        primary key,
    carMake_id int          null,
    name       varchar(255) not null,
    constraint car_model_car_make_id_fk
        foreign key (carMake_id) references car_make (id)
);

INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (9, 0, '156');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (14, 0, '159');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (20, 0, '145');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (27, 0, '146');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (35, 0, '147');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (44, 0, '155');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (54, 0, '164');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (65, 0, '166');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (77, 0, 'Brera');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (90, 0, 'GT');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (104, 0, 'GTV');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (119, 0, 'Spider');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (135, 0, '33');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (152, 0, '75');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (153, 1, 'DB7');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (155, 1, 'DB9');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (158, 1, 'DBS');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (162, 1, 'Rapide');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (167, 1, 'Vanquish');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (173, 1, 'Vantage');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (180, 1, 'Virage');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (181, 2, 'A3');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (183, 2, 'A4');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (186, 2, 'A5');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (190, 2, 'A6');

create table multicriteria_table
(
    id          int    not null
        primary key,
    cc_min      int    null,
    cc_max      int    null,
    car_age_min int    null,
    car_age_max int    null,
    base_amount double null
);

INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (1, 0, 1047, 0, 19, 403.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (2, 0, 1047, 20, 999, 413.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (3, 1048, 1309, 0, 19, 529.63);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (4, 1048, 1309, 20, 999, 539.63);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (5, 1310, 2356, 0, 19, 690.96);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (6, 1310, 2356, 20, 999, 700.96);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (7, 2357, 2880, 0, 19, 862.86);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (8, 2357, 2880, 20, 999, 892.86);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (9, 2881, 4188, 0, 19, 957.89);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (10, 2881, 4188, 20, 999, 987.89);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (11, 4189, 5497, 0, 19, 1076.62);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (12, 4189, 5497, 20, 999, 1106.62);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (13, 5498, 999999, 0, 19, 1193.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (14, 5498, 999999, 20, 999, 1263.25);

create table users
(
    username varchar(50)  not null
        primary key,
    password varchar(100) not null,
    enabled  tinyint      not null
);

INSERT INTO safetycar.users (username, password, enabled) VALUES ('viktorpetrov977@gmail.com', '$2a$10$Vcs.wtMd1t5W6iAnqADiDulZfVPgiyWJsVmBpQOQw/RuMMWpFgaB.', 1);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.authorities (username, authority) VALUES ('viktorpetrov977@gmail.com', 'ROLE_USER');

create table confirmation_token
(
    id          int auto_increment
        primary key,
    token       varchar(50) not null,
    username    varchar(50) not null,
    expiry_date datetime    not null,
    constraint confirmation_token_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.confirmation_token (id, token, username, expiry_date) VALUES (10, '6643a486-d154-41c0-8704-6f1065e56c04', 'viktorpetrov977@gmail.com', '2020-10-16 12:06:53');

create table user_details
(
    id           int auto_increment
        primary key,
    username     varchar(50)  null,
    first_name   varchar(50)  not null,
    last_name    varchar(50)  not null,
    picture      blob         null,
    phone_number varchar(50)  null,
    address      varchar(255) null,
    birth_date   date         null,
    constraint user_details_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.user_details (id, username, first_name, last_name, picture, phone_number, address, birth_date) VALUES (19, 'viktorpetrov977@gmail.com', 'Viktor', 'Petrov', null, null, null, null);

create table if not exists safetycar.policy_request
(
    id                      int auto_increment
        primary key,
    effective_date          date        not null,
    user_id                 int         not null,
    status                  varchar(25) null,
    vehicle_image_id        int         null,
    accident_prev_year      tinyint(1)  not null,
    first_registration_date date        not null,
    driver_age              int         not null,
    cubic_capacity          int         not null,
    totalPremium            double      not null,
    carModel_id             int         null,
    constraint policy_request_car_model_id_fk
        foreign key (carModel_id) references safetycar.car_model (id),
    constraint policy_request_user_details_id_fk
        foreign key (user_id) references safetycar.user_details (id),
    constraint policy_request_vehicle_image_id_fk
        foreign key (vehicle_image_id) references safetycar.vehicle_image (id)
);

create index policy_request_request_status_id_fk
    on safetycar.policy_request (status);

INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, vehicle_image_id, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (9, '2020-10-28', 19, 'PENDING', null, 1, '2020-10-13', 20, 1500, 951, 34792);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, vehicle_image_id, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (11, '2020-11-07', 19, 'PENDING', null, 1, '2020-10-13', 20, 1500, 951, 34795);

create table if not exists safetycar.vehicle_image
(
    id    int auto_increment
        primary key,
    image longblob    null,
    name  varchar(20) null,
    type  varchar(20) null
);

