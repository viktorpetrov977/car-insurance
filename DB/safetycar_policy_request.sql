create table policy_request
(
    id                      int auto_increment
        primary key,
    effective_date          date        not null,
    user_id                 int         not null,
    status                  varchar(25) null,
    car_image               blob        null,
    accident_prev_year      tinyint(1)  not null,
    first_registration_date date        not null,
    driver_age              int         not null,
    cubic_capacity          int         not null,
    totalPremium            double      not null,
    carModel_id             int         null,
    constraint policy_request_car_model_id_fk
        foreign key (carModel_id) references car_model (id),
    constraint policy_request_user_details_id_fk
        foreign key (user_id) references user_details (id)
);

create index policy_request_request_status_id_fk
    on policy_request (status);

INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (3, '2020-10-26', 19, 'PENDING', null, 1, '2020-10-12', 20, 1500, 951, 43485);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (4, '2020-11-03', 19, 'PENDING', null, 0, '2020-10-06', 25, 1500, 761, 43486);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (5, '2020-10-30', 19, 'PENDING', null, 1, '2020-09-30', 26, 1500, 913, 43487);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (6, '2020-11-07', 19, 'PENDING', null, 1, '2020-10-05', 35, 1500, 913, 43488);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (7, '2020-10-27', 19, 'PENDING', null, 1, '2020-10-14', 25, 1500, 913, 43489);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (8, '2020-10-30', 19, 'PENDING', null, 1, '2020-10-07', 25, 15000, 1576, 43490);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (9, '2020-10-28', 19, 'PENDING', null, 1, '2020-10-13', 20, 1500, 951, 34792);
INSERT INTO safetycar.policy_request (id, effective_date, user_id, status, car_image, accident_prev_year, first_registration_date, driver_age, cubic_capacity, totalPremium, carModel_id) VALUES (11, '2020-11-07', 19, 'PENDING', null, 1, '2020-10-13', 20, 1500, 951, 34795);