create table multicriteria_table
(
    id          int    not null
        primary key,
    cc_min      int    null,
    cc_max      int    null,
    car_age_min int    null,
    car_age_max int    null,
    base_amount double null
);

INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (1, 0, 1047, 0, 19, 403.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (2, 0, 1047, 20, 999, 413.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (3, 1048, 1309, 0, 19, 529.63);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (4, 1048, 1309, 20, 999, 539.63);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (5, 1310, 2356, 0, 19, 690.96);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (6, 1310, 2356, 20, 999, 700.96);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (7, 2357, 2880, 0, 19, 862.86);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (8, 2357, 2880, 20, 999, 892.86);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (9, 2881, 4188, 0, 19, 957.89);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (10, 2881, 4188, 20, 999, 987.89);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (11, 4189, 5497, 0, 19, 1076.62);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (12, 4189, 5497, 20, 999, 1106.62);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (13, 5498, 999999, 0, 19, 1193.25);
INSERT INTO safetycar.multicriteria_table (id, cc_min, cc_max, car_age_min, car_age_max, base_amount) VALUES (14, 5498, 999999, 20, 999, 1263.25);