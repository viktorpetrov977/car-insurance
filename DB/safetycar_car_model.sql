create table car_model
(
    id         int          not null
        primary key,
    carMake_id int          null,
    name       varchar(255) not null,
    constraint car_model_car_make_id_fk
        foreign key (carMake_id) references car_make (id)
);

INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (9, 0, '156');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (14, 0, '159');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (20, 0, '145');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (27, 0, '146');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (35, 0, '147');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (44, 0, '155');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (54, 0, '164');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (65, 0, '166');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (77, 0, 'Brera');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (90, 0, 'GT');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (104, 0, 'GTV');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (119, 0, 'Spider');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (135, 0, '33');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (152, 0, '75');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (153, 1, 'DB7');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (155, 1, 'DB9');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (158, 1, 'DBS');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (162, 1, 'Rapide');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (167, 1, 'Vanquish');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (173, 1, 'Vantage');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (180, 1, 'Virage');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (181, 2, 'A3');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (183, 2, 'A4');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (186, 2, 'A5');
INSERT INTO safetycar.car_model (id, carMake_id, name) VALUES (190, 2, 'A6');