create table users
(
    username varchar(50)  not null
        primary key,
    password varchar(100) not null,
    enabled  tinyint      not null
);

INSERT INTO safetycar.users (username, password, enabled) VALUES ('newtestUser@test.com', '{noop}newtest', 1);
INSERT INTO safetycar.users (username, password, enabled) VALUES ('testUser@test.com', '{noop}test', 1);
INSERT INTO safetycar.users (username, password, enabled) VALUES ('viktorpetrov977@gmail.com', '$2a$10$Vcs.wtMd1t5W6iAnqADiDulZfVPgiyWJsVmBpQOQw/RuMMWpFgaB.', 1);