create table user_details
(
    id           int auto_increment
        primary key,
    username     varchar(50)  null,
    first_name   varchar(50)  not null,
    last_name    varchar(50)  not null,
    picture      blob         null,
    phone_number varchar(50)  null,
    address      varchar(255) null,
    birth_date   date         null,
    constraint user_details_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.user_details (id, username, first_name, last_name, picture, phone_number, address, birth_date) VALUES (3, 'testUser@test.com', 'test', 'user', null, '000-000-0000', 'BG SF', '2000-01-01');
INSERT INTO safetycar.user_details (id, username, first_name, last_name, picture, phone_number, address, birth_date) VALUES (4, 'newtestUser@test.com', 'newtest', 'newuser', null, null, null, null);
INSERT INTO safetycar.user_details (id, username, first_name, last_name, picture, phone_number, address, birth_date) VALUES (19, 'viktorpetrov977@gmail.com', 'Viktor', 'Petrov', null, null, null, null);