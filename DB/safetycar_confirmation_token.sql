create table confirmation_token
(
    id          int auto_increment
        primary key,
    token       varchar(50) not null,
    username    varchar(50) not null,
    expiry_date datetime    not null,
    constraint confirmation_token_users_username_fk
        foreign key (username) references users (username)
);

INSERT INTO safetycar.confirmation_token (id, token, username, expiry_date) VALUES (10, '6643a486-d154-41c0-8704-6f1065e56c04', 'viktorpetrov977@gmail.com', '2020-10-16 12:06:53');