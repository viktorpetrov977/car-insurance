create table car_make
(
    id   int          not null
        primary key,
    name varchar(255) not null
);

INSERT INTO safetycar.car_make (id, name) VALUES (0, 'Alfa Romeo');
INSERT INTO safetycar.car_make (id, name) VALUES (1, 'Aston Martin');
INSERT INTO safetycar.car_make (id, name) VALUES (2, 'Audi');
INSERT INTO safetycar.car_make (id, name) VALUES (3, 'Bentley');
INSERT INTO safetycar.car_make (id, name) VALUES (4, 'BMW');
INSERT INTO safetycar.car_make (id, name) VALUES (5, 'Cadillac');
INSERT INTO safetycar.car_make (id, name) VALUES (6, 'Chery');
INSERT INTO safetycar.car_make (id, name) VALUES (7, 'Chevrolet');
INSERT INTO safetycar.car_make (id, name) VALUES (8, 'Chrysler');
INSERT INTO safetycar.car_make (id, name) VALUES (9, 'Citroen');
INSERT INTO safetycar.car_make (id, name) VALUES (10, 'Dacia');
INSERT INTO safetycar.car_make (id, name) VALUES (11, 'Daewoo');
INSERT INTO safetycar.car_make (id, name) VALUES (12, 'Daihatsu');
INSERT INTO safetycar.car_make (id, name) VALUES (13, 'DFM');
INSERT INTO safetycar.car_make (id, name) VALUES (14, 'Dodge');
INSERT INTO safetycar.car_make (id, name) VALUES (15, 'Ferrari');
INSERT INTO safetycar.car_make (id, name) VALUES (16, 'Fiat');
INSERT INTO safetycar.car_make (id, name) VALUES (17, 'Ford');
INSERT INTO safetycar.car_make (id, name) VALUES (18, 'Geely');
INSERT INTO safetycar.car_make (id, name) VALUES (19, 'Honda');
INSERT INTO safetycar.car_make (id, name) VALUES (20, 'Hyundai');
INSERT INTO safetycar.car_make (id, name) VALUES (21, 'Infiniti');
INSERT INTO safetycar.car_make (id, name) VALUES (22, 'Isuzu');
INSERT INTO safetycar.car_make (id, name) VALUES (23, 'Jaguar');
INSERT INTO safetycar.car_make (id, name) VALUES (24, 'Jeep');
INSERT INTO safetycar.car_make (id, name) VALUES (25, 'Kia');
INSERT INTO safetycar.car_make (id, name) VALUES (26, 'Lada');
INSERT INTO safetycar.car_make (id, name) VALUES (27, 'Lamborghini');
INSERT INTO safetycar.car_make (id, name) VALUES (28, 'Lancia');
INSERT INTO safetycar.car_make (id, name) VALUES (29, 'Land Rover');
INSERT INTO safetycar.car_make (id, name) VALUES (30, 'Maserati');
INSERT INTO safetycar.car_make (id, name) VALUES (31, 'Mazda');
INSERT INTO safetycar.car_make (id, name) VALUES (32, 'Mercedes');
INSERT INTO safetycar.car_make (id, name) VALUES (33, 'Mini');
INSERT INTO safetycar.car_make (id, name) VALUES (34, 'Mitsubishi');
INSERT INTO safetycar.car_make (id, name) VALUES (35, 'Nissan');
INSERT INTO safetycar.car_make (id, name) VALUES (36, 'Opel');
INSERT INTO safetycar.car_make (id, name) VALUES (37, 'Peugeot');
INSERT INTO safetycar.car_make (id, name) VALUES (38, 'Porsche');
INSERT INTO safetycar.car_make (id, name) VALUES (39, 'Proton');
INSERT INTO safetycar.car_make (id, name) VALUES (40, 'Renault');
INSERT INTO safetycar.car_make (id, name) VALUES (41, 'Rover');
INSERT INTO safetycar.car_make (id, name) VALUES (42, 'Saab');
INSERT INTO safetycar.car_make (id, name) VALUES (43, 'Seat');
INSERT INTO safetycar.car_make (id, name) VALUES (44, 'Skoda');
INSERT INTO safetycar.car_make (id, name) VALUES (45, 'Smart');
INSERT INTO safetycar.car_make (id, name) VALUES (46, 'SsangYong');
INSERT INTO safetycar.car_make (id, name) VALUES (47, 'Subaru');
INSERT INTO safetycar.car_make (id, name) VALUES (48, 'Suzuki');
INSERT INTO safetycar.car_make (id, name) VALUES (49, 'Tata');
INSERT INTO safetycar.car_make (id, name) VALUES (50, 'TofaE');
INSERT INTO safetycar.car_make (id, name) VALUES (51, 'Toyota');
INSERT INTO safetycar.car_make (id, name) VALUES (52, 'Volkswagen');
INSERT INTO safetycar.car_make (id, name) VALUES (53, 'Volvo');